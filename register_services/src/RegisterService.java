

import java.io.IOException;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;


public class RegisterService {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Activate these lines to see log messages of JmDNS
        Logger logger = Logger.getLogger(JmDNS.class.getName());
        ConsoleHandler handler = new ConsoleHandler();
        logger.addHandler(handler);
        logger.setLevel(Level.FINER);
        handler.setLevel(Level.FINER);
        */
        
        try {
            System.out.println("Opening JmDNS");
            JmDNS jmdns = JmDNS.create();
            System.out.println("Opened JmDNS");
            System.out.println("\nPress r and Enter, to register HTML service 'foo'");
            int b;
            while ((b = System.in.read()) != -1 && (char) b != 'r');
            ServiceInfo info = ServiceInfo.create("_http._tcp.local.", "foo", 1268, 0, 0, "path=index.html");
            jmdns.registerService(info);
            
            System.out.println("\nRegistered Service as "+info);
            System.out.println("Press q and Enter, to quit");
            //int b;
            while ((b = System.in.read()) != -1 && (char) b != 'q'); 
            System.out.println("Closing JmDNS");
            jmdns.close();
            System.out.println("Done");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}